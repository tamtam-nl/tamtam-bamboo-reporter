tamtam-bamboo-reporter
=====================

A karma reporter plugin to create a mocha.json file so Bamboo can read the testresults


Install
------------
`npm install tamtam-bamboo-reporter`

Usage
------------
```
...

reporters: ['bamboo'],

bambooReporter:{
    filename: 'util.mocha.json' //optional, defaults to "mocha.json"
},

...
```